import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchTokens } from './API/btcTokens';
import './App.css';
import Bitcoin from './components/btcInfo/Bitcoin';
import Loader from './components/loader/Loader';

function App() {
  const dispatch = useDispatch();
  const tokens = useSelector(state => state.btcReducer.tokens);
  const isFetching = useSelector(state => state.btcReducer.isFetching);

  useEffect(() => {
    dispatch(fetchTokens());
  }, [])

  
  return (
    <div className="App">      
      {isFetching === false
        ?
        <Bitcoin tokens= {tokens}/>
        :
        <Loader/>
      }
    </div>
  );
}

export default App;
