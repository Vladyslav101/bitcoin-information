import { setIsFetching, setTokens } from "../store/btcReducer";


export const fetchTokens = () => {

    return async function (dispatch) {
        try {
            dispatch(setIsFetching(true))
            await fetch('https://api.coindesk.com/v1/bpi/currentprice.json')
                .then(response => response.json())
                .then(json => dispatch(setTokens(json)))
        } catch (e) {
            alert(e);
        } finally {
            dispatch(setIsFetching(false))
        } 
          
    }
}