import React from 'react';
import { useDispatch } from 'react-redux';
import { fetchTokens } from '../../API/btcTokens';
import Mybutton from '../button/MyButton';
import './Bitcoin.scss';

const Bitcoin = (props) => {
    const token = props.tokens;
    const dispatch = useDispatch();
     
    const refreshCur = () => {
        dispatch(fetchTokens());
    }

    const fixNum = (num) => num.toFixed(2);

    return (
        <article className="btc">
            <h1 className='btc__title'>Bitcoin currency info</h1>
            <p className='btc__currency'>1 {token.chartName} - {fixNum(token.bpi.EUR.rate_float)} {token.bpi.EUR.description} </p>
            <p className='btc__currency'>1 {token.chartName} - {fixNum(token.bpi.USD.rate_float)} {token.bpi.USD.description}</p>
            <p className='btc__currency'>1 {token.chartName} - {fixNum(token.bpi.GBP.rate_float)} {token.bpi.GBP.description}</p>
            <p className='btc__updated'>Last updated at: {token.time.updated}</p>
            <Mybutton onClick={refreshCur}>Refresh currency</Mybutton>
        </article>
    );
};

export default Bitcoin;